package com.casumo.videorentalstore.rental.data

enum class Price(val amount: Int) {
    PREMIUM(40), BASIC(30)
}